if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

_sshcd_complete() {
    local cur
    _get_comp_words_by_ref -n: cur
    COMPREPLY=( $(compgen -W "$(cat ~/.sshcd_history)" -- $cur) )
    __ltrim_colon_completions "$cur"
}
complete -F _sshcd_complete sshcd
complete -F _sshcd_complete SshfsVscode
complete -F _sshcd_complete RemoteVscode
