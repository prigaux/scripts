(global-set-key (kbd "<S-delete>")
   (lambda () (interactive)
     (if (use-region-p) (kill-region (region-beginning) (region-end)) (kill-whole-line))
     ))
