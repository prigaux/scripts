;; load ~/.sshcd_history into emacs bookmarks
;;
;; usage: (load "~/xxx/scripts/.emacs.d/bookmarks-use-sshcd-history.el")

(defadvice bookmark-load (after bookmark-load activate)
  (let ((filenames (mapcar 'bookmark-get-filename bookmark-alist))
        (toadd (with-temp-buffer
                 (insert-file-contents "~/.sshcd_history")
                 (split-string (buffer-string) "\n" t))))
    (dolist (name toadd)
      (let ((filename (concat "/scp:" name)))
        (if (not (member filename filenames))
            (add-to-list 'bookmark-alist (list name `((filename . ,filename)))))))))
